class GenerateList
  def list
    (1..16).to_a
  end

  def print
    list = (1..100).to_a
    list.map do |number|
      if fizzbuzz?(number)
        'fizzbuzz'
      elsif buzz?(number)
        'buzz'
      elsif fizz?(number)
        'fizz'
      else
        number
      end
    end
  end

  def fizz?(number)
    number % 3 == 0
  end

  def buzz?(number)
    number % 5 == 0
  end

  def fizzbuzz?(number)
    number % 3 == 0 && number % 5 == 0
  end
end

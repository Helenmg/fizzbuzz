require 'rspec'

require_relative '../fizzbuzz'

describe 'yellow belt' do
  it 'List the numbers from 1 to 100' do
    # arrenge
    list = GenerateList.new

    # act
    size = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]

    #assert
    expect(list.list).to eq(size)
  end
  it 'if the number is divisible by 3, write "Fizz" instead' do
    # arrenge
    MULTIPLE_OF_3 = 3
    list = GenerateList.new
    # act
    result = list.fizz?(MULTIPLE_OF_3)

    #assert
    expect(result).to eq(true)
  end
  it 'if the number is divisible by 5, write "Buzz" instead' do
    # arrenge
    MULTIPLE_OF_5 = 5
    list = GenerateList.new
    # act
    result = list.buzz?(MULTIPLE_OF_5)

    #assert
    expect(result).to eq(true)
  end
  it 'If the number is divisible by both 3 and 5, write "FizzBuzz"' do
    # arrenge
    MULTIPLE_OF_3_AND_5 = 15
    list = GenerateList.new
    # act
    result = list.fizzbuzz?(MULTIPLE_OF_3_AND_5)

    #assert
    expect(result).to eq(true)
  end
  it 'Print the numbers from 1 to 100' do
    # arrenge
    list = GenerateList.new

    # act
    result = list.print

    #assert
    expect(result[0]).to eq(1)
    expect(result[1]).to eq(2)
    expect(result[2]).to eq('fizz')
    expect(result[4]).to eq('buzz')
    expect(result[14]).to eq('fizzbuzz')
  end
end
